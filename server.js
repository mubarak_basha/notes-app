require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const app = express()
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(express.static('public'))
app.set('view engine', 'pug')

//load models
require('./models/note')

//initiate models
const Note = mongoose.model('Note')

mongoose.connect(process.env.MONGODB_CONNECTION_STRING, {useNewUrlParser: true})
  .then(() => {
    console.log('Connected to MongoDB database note-app')
  },
  err => { console.error(`Error connecting to mongodb ${err}`) }
  )

app.get('/', async (req, res) => {
  const notes = await Note.find({}).sort({createdOn: -1})
  res.render('index', {notes})
})

app.get('/note/:id?', async (req, res) => {
  const id = req.params.id
  let note = null
  if(id) {
    note = await Note.findOne({_id: id})
    console.log(note)
  }
  res.render('note', {note})
})

app.post('/savenote', async (req, res) => {
  const id = req.body.noteId
  let note = null
  if(id) {
    note = await Note.findOne({_id: id})
  }

  if(!note){
    note = new Note()
  }
  note.content = req.body.noteContent
  note.bgColor = req.body.noteBg
  note.save()
    .then(dbNote => {
      res.send({success: true, data: dbNote})
    })
    .catch(err => {
      res.send({success: false, message: err})
    })
})

app.get('/delete/:id', (req, res) => {
  Note.findOneAndDelete({_id: req.params.id})
    .then(() => {
      res.redirect('/')
    })
})

const PORT = 3002
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`)
})